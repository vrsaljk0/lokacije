# Lokacije - Kringing interpolacija TEC globalne mape

Otvaranje projekta u RStudio-u:

File -> Open Project -> `lokacije.RStudio`


Pokretanje GUI-a

1. Potrebno je instalirati pakete:
    - `dplyr`
    - `shiny`
    - `sp`
    - `gstat`
    - `rnaturalearth`
    - `rnaturalearthdata`
    - `ggplot2`
    - `scales`
    - `magrittr`

2. Postaviti svoj path u datotekama `ui.R` i `helper.R `
  
    `helper.R` se **ne pokreće**

3. RStudio prepoznaje strukturu shiny aplikacije i nudi `Run App` bottun. 
    
    Alternativa; pokretanje iz konzole (iz `~/gui` direktorija):
    ```
    library(shiny)
    runApp("gui")
    ```

Interpolacija Krigingom traje između 1 i 5min.
    
Demo: https://youtu.be/s9mqEuxCWpg

